<!DOCTYPE html>
<!--
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHPWebPage.php to edit this template
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $entero=10;
            $cadena="hola";
            $real=23.6;
            $logico=TRUE;
            
            var_dump($entero);
            var_dump($cadena);
            var_dump($real);
            var_dump($logico);
            
            $logico=(int)$logico;
            $entero=(float)$entero;
            settype($logico, "int");
            
            var_dump($entero);
            var_dump($cadena);
            var_dump($real);
            var_dump($logico);
        ?>
        
    </body>
</html>
